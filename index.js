const { Kafka } = require("kafkajs");

const clientId = 'express-mongo'
const topic = 'test-kafka'

const kafka = new Kafka({
    clientId: clientId,
    brokers: ['localhost:9092']
})

const consumer = kafka.consumer({ groupId: clientId })

const consume = async () => {
    // first, we wait for the client to connect and subscribe to the given topic
    await consumer.connect()
    await consumer.subscribe({ topic: 'test-kafka', fromBeginning: true })
    await consumer.run({
        eachMessage: async ({ topic, partition, message }) => {
            console.log(JSON.parse(message.value.toString()))
        },
    })
}

consume()